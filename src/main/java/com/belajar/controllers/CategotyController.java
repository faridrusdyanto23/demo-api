package com.belajar.controllers;

import java.util.Arrays;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.belajar.dto.CategoryData;
import com.belajar.dto.ResponseData;
import com.belajar.dto.SearchData;
import com.belajar.models.entities.Category;
import com.belajar.services.CategorySercive;

@RestController
@RequestMapping("/api/categories")
public class CategotyController {

  @Autowired
  private CategorySercive categorySercive;

  @Autowired
  private ModelMapper modelMapper;

  @PostMapping
  public ResponseEntity<ResponseData<Category>> create(@Valid @RequestBody CategoryData categoryData, Errors errors) {
    ResponseData<Category> responseData = new ResponseData<>();
    if (errors.hasErrors()) {
      for (ObjectError error : errors.getAllErrors()) {
        responseData.getMessages().add(error.getDefaultMessage());
      }
      responseData.setStatus(false);
      responseData.setPayload(null);
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
    }
    Category category = modelMapper.map(categoryData, Category.class);
    responseData.setStatus(true);
    responseData.setPayload(categorySercive.save(category));
    return ResponseEntity.ok(responseData);
  }

  @GetMapping
  public Iterable<Category> findAll() {
    return categorySercive.findAll();
  }

  @GetMapping("/{id}")
  public Category findOne(@PathVariable("id") Long id) {
    return categorySercive.findOne(id);
  }

  @PutMapping
  public ResponseEntity<ResponseData<Category>> update(@Valid @RequestBody Category category, Errors errors) {
    ResponseData<Category> responseData = new ResponseData<>();

    if (errors.hasErrors()) {
      for (ObjectError error : errors.getAllErrors()) {
        responseData.getMessages().add(error.getDefaultMessage());
      }
      responseData.setStatus(false);
      responseData.setPayload(null);
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
    }

    // Tidak memerlukan ini karena update membutuhkan id
    // Category category = modelMapper.map(categoryData, Category.class);
    responseData.setStatus(true);
    responseData.setPayload(categorySercive.save(category));
    return ResponseEntity.ok(responseData);
  }

  @PostMapping("/search/{size}/{page}")
  public Iterable<Category> findByName(
      @RequestBody SearchData searchData,
      @PathVariable("size") int size,
      @PathVariable("page") int page) {

    Pageable pageable = PageRequest.of(page, size);

    return categorySercive.findByName(searchData.getSearchKey(), pageable);
  }

  @PostMapping("/search/{size}/{page}/{sort}")
  public Iterable<Category> findByName(
      @RequestBody SearchData searchData,
      @PathVariable("size") int size,
      @PathVariable("page") int page,
      @PathVariable("sort") String sort) {

        Pageable pageable = PageRequest.of(page, size, Sort.by("id"));
    if (sort.equalsIgnoreCase("desc")) {
      pageable = PageRequest.of(page, size, Sort.by("id").descending());
    }
    return categorySercive.findByName(searchData.getSearchKey(), pageable);
  }

  @PostMapping("/batch")
  public ResponseEntity<ResponseData<Iterable<Category>>> createBatch(@RequestBody Category[] categories) {
    ResponseData<Iterable<Category>> responseData = new ResponseData<>();
    responseData.setPayload(categorySercive.saveBatch(Arrays.asList(categories)));
    responseData.setStatus(true);
    return ResponseEntity.ok(responseData);
  }
}
