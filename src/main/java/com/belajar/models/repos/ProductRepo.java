package com.belajar.models.repos;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.belajar.models.entities.Product;
import com.belajar.models.entities.Supplier;

import java.util.List;

import javax.websocket.server.PathParam;

public interface ProductRepo extends CrudRepository<Product, Long>{
  List<Product> findByNameContains(String name);

  // :name bisa bebas asalkan harus sama dengan @Param
  @Query("SELECT p FROM Product p WHERE p.name = :name")
  public Product findProductByName(@Param("name") String name);

  @Query("SELECT p FROM Product p WHERE p.name LIKE :name")
  public List<Product> findProductByNameLike(@PathParam("name") String name);
  
  @Query("SELECT p FROM Product p WHERE p.category.id = :categoryId")
  public List<Product> findProductByCategory(@PathParam("categoryId") Long categoryId);

  @Query("SELECT p FROM Product p WHERE :supplier MEMBER OF p.suppliers")
  public List<Product> findProductBySupplier(@PathParam("supplier") Supplier supplier);
}
