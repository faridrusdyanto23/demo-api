package com.belajar.models.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.belajar.models.entities.Supplier;

public interface SupplierRepo extends CrudRepository<Supplier, Long> {
  
  Supplier findByEmail(String email);
  
  List<Supplier> findByNameContainsOrderByIdDesc(String name);

  List<Supplier> findByNameStartingWith(String prefix);

  List<Supplier> findByNameContainsOrEmailContains(String name, String email);
}
